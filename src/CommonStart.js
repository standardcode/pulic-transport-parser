import Parser from './Parser';

export default (settings)=> {
	const parser = new Parser();
	parser.update(settings.urlProvider);
}
