export default {
	path: 'mpk_linie_wgs',
	collection: 'lines_tmp',
	unique: (p)=> p.id,
	couchConverter: (p)=> {
		return {
			id: p.id,
			type: p.type,
			geometry: p.geometry,
			properties: {
				from: p.properties.stop_from,
				to: p.properties.stop_to,
				line: p.properties.a3,
				type: p.properties.a4, // B, T, N (night bus), X (night tram)
				direction: p.properties.a5 == "1",
				order: parseInt(p.properties.a6)
			}
		}
	}
}
