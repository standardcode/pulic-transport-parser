"use strict";

const request = require("request");

import stations from './stations';
import lines from './lines';

export default
class Parser {
	constructor() {
	}

	update(urlProvider) {
		const self = this;
		let saved = 0, total = 0;
		let savedLayers = 0;
		const layers = ['stations', 'lines'];
		const layersLength = layers.length;

		function parseFile(file, handler) {
			request(urlProvider(file), (error, response, body)=> {
				console.log("Response for " + file + ", error " + error + ", length " + (body && body.length));
				handler(JSON.parse(body).features);
			});
		}

		function updateItems(layer, features) {
			total += features.length;
			++savedLayers;
			request.put({
				uri: "http://localhost:5984/" + layer.collection,
				json: {}
			}, ()=> features.forEach((feature)=> db(layer, feature)));
		}

		function db(layer, raw) {
			request.post({
				uri: "http://localhost:5984/" + layer.collection,
				json: layer.couchConverter(raw)
			}, ()=> {
				saved++;
				if (savedLayers == layersLength && saved >= total) {
					console.log('Saved everything');
					self.shutdown();
				}
			});
		}

		[stations, lines].forEach((layer)=> {
			console.log(layer.path);
			parseFile(layer.path, (features)=> updateItems(layer, features));
		});
	}

	shutdown() {
		console.log("End");
	}
}