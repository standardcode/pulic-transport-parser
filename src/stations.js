import Util from './util';

export default {
	path: 'mpk_przystanki_wgs',
	collection: 'stations',
	unique: (p)=>p.id,
	couchConverter: (p)=> {
		return {
			id: p.id,
			type: p.type,
			geometry: p.geometry,
			properties: {
				name: p.properties.a3,
				type: p.properties.a4, // A or T
				lines: p.properties.a2.split(','),
				tiles: Util.tiles(p.geometry.coordinates)
			}
		}
	}
}
