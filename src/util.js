const _ = require('lodash');

const methods = {
	toTiles: (lng_deg, lat_deg, zoom) => {
		var x = (Math.floor((lng_deg + 180) / 360 * Math.pow(2, zoom)));
		var y = (Math.floor((1 - Math.log(Math.tan(lat_deg * Math.PI / 180) + 1 / Math.cos(lat_deg * Math.PI / 180)) / Math.PI) / 2 * Math.pow(2, zoom)));

		return {
			x,
			y
		};
	},

	tiles: (coordinates)=> {
		return _.range(18).map((zoom)=>methods.toTiles(coordinates[0], coordinates[1], zoom))
	}
};

export default methods;