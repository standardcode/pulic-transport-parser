'use strict';

var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var traceur = require('gulp-traceur');

gulp.task('build', ['scripts']);

gulp.task('default', ['clean'], function () {
	gulp.start('build');
});

gulp.task('scripts', function () {
	return gulp.src('src/*.js')
		.pipe(traceur({
			sourceMap: true,
			blockBinding: true
		}))
		.pipe(gulp.dest('dist'));
});

gulp.task('clean', function () {
	return gulp.src(['.tmp', 'dist'], { read: false }).pipe($.clean());
});

gulp.task('watch', function () {
	gulp.watch('src/**/*.js', ['scripts']);
});
